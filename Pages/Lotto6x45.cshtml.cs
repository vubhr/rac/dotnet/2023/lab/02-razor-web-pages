﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Lottery.Pages;

public class Lotto6x45Model : PageModel
{
    public List<int> Brojevi { get; set;}

    private readonly ILogger<Lotto6x45Model> _logger;

    public Lotto6x45Model(ILogger<Lotto6x45Model> logger)
    {
        _logger = logger;
        Brojevi = new List<int>();
    }

    public void OnGet()
    {
        Izgeneriraj6Brojeva();
    }

    private void Izgeneriraj6Brojeva()
    {
        var random = new Random();
        for (int i = 0; i < 6; i++)
        {
            Brojevi.Add(random.Next(1, 46));
        }
        Brojevi.Sort();
    }
}
